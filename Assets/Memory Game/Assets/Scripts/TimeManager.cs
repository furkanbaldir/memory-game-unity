﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    private float time;
    private int counterTime;
    [SerializeField] private TMP_Text timeText;
    private bool control;
    void Start()
    {
        time = 0;
        counterTime = 1;
        control = true;
    }

    // Update is called once per frame
    void Update()
    {
        time = time + (Time.deltaTime * counterTime);
        if (control)
        {
            timeText.text = string.Format("{0:#} second", time);
        }
       
    }

    public void ResetTime()
    {
        time = 0;
    }

    public void PauseTime()
    {
        counterTime = 0;
    }

    public void ContinueTime()
    {
        counterTime = 1;
    }

    public float GetTime()
    {
        return time;
    }

    public void InactiveTimeObject()
    {
        control = false;
        timeText.text = "";
    }
}
