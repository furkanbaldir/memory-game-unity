﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    // Best results
    private int bestAttempt_6;
    private float bestTime_6;
    private int bestAttempt_12;
    private float bestTime_12;
    private int bestAttempt_18;
    private float bestTime_18;

    // HighScores Panel Texts
    [SerializeField] private TMP_Text bestAttemptText_6;
    [SerializeField] private TMP_Text bestTimeText_6;
    [SerializeField] private TMP_Text bestAttemptText_12;
    [SerializeField] private TMP_Text bestTimeText_12;
    [SerializeField] private TMP_Text bestAttemptText_18;
    [SerializeField] private TMP_Text bestTimeText_18;

    private GameObject[] cards = new GameObject[18];                                // Cards array to use cards specifically
    [SerializeField] private GameObject cardsInHierarchy;                           // In hierarchy, Cards parent object of 18 card game objects

    // Index arrays to set active card objects
    private int[] indexesStart6 = { 2, 3, 8, 9, 14, 15 };                           // Start with 6 cards
    private int[] indexesStart12 = { 1, 2, 3, 4, 7, 8, 9, 10, 13, 14, 15, 16 };     // Start with 12 cards

    // Panels
    [SerializeField] private GameObject transitionPanel;
    [SerializeField] private GameObject menuPanel;
    [SerializeField] private GameObject finishGamePanel;
    [SerializeField] private GameObject escMenu;
    [SerializeField] private GameObject highScoresPanel;
    private bool escMenuEnabled;

    // UI
    //[SerializeField] private Text timeText;
    private int succeedAttempt;
    private int startNumber;
    [SerializeField] private GameObject escMenuButton;

    [SerializeField] private TMP_Text finishAttempt;
    [SerializeField] private TMP_Text finishTime;

    [SerializeField] private TMP_Text attemptText;
    private int attempt;

    // Card Sprites
    [SerializeField] private Sprite cardFront;
    [SerializeField] private Sprite cardBack;

    // Check opening cards and limit to 2 cards same time
    private int selectedCards;
    private GameObject[] selectedCardsArray = new GameObject[2];

    // Time manager
    [SerializeField] private GameObject timeManager;


    void Start()
    {
        // Append all card gameobjects to cards array 
        for (int i = 0; i < 18; i++)
        {
            cards[i] = cardsInHierarchy.transform.GetChild(i).gameObject;
        }
        selectedCards = 0;
        escMenuEnabled = false;
        attempt = 0;
        succeedAttempt = 0;
    }

    private void Update()
    {
        // ESC menu using with keyboard_esc
        if (Input.GetKeyDown(KeyCode.Escape) && !escMenuEnabled)
        {
            PauseGame();

        }
        else if (Input.GetKeyDown(KeyCode.Escape) && escMenuEnabled)
        {
            ContinueGame();
        }
    }
    // Continue game
    public void ContinueGame()
    {
        escMenuEnabled = false;
        transitionPanel.SetActive(false);
        escMenu.SetActive(false);
        InteractableOpenButtons();
        Time.timeScale = 1;
    }

    // Pause game
    public void PauseGame()
    {
        escMenuEnabled = true;
        transitionPanel.SetActive(true);
        escMenu.SetActive(true);
        InteractableCloseButtons();
        Time.timeScale = 0;
    }

    // Restart game
    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MemoryGame");
    }

    // Exit game
    public void ExitGame()
    {
        Application.Quit();
    }

    public void HighScoresButton()
    {
        highScoresPanel.SetActive(true);
        menuPanel.SetActive(false);

        bestAttempt_6 = PlayerPrefs.GetInt("MAX_ATTEMPT_6");
        bestTime_6 = PlayerPrefs.GetFloat("MAX_TIME_6");
        bestAttempt_12 = PlayerPrefs.GetInt("MAX_ATTEMPT_12");
        bestTime_12 = PlayerPrefs.GetFloat("MAX_TIME_12");
        bestAttempt_18 = PlayerPrefs.GetInt("MAX_ATTEMPT_18");
        bestTime_18 = PlayerPrefs.GetFloat("MAX_TIME_18");

        bestAttemptText_6.text = "Attempt: " + bestAttempt_6;
        bestTimeText_6.text = "Time: " + bestTime_6;
        bestAttemptText_12.text = "Attempt: " + bestAttempt_12;
        bestTimeText_12.text = "Time: " + bestTime_12;
        bestAttemptText_18.text = "Attempt: " + bestAttempt_18;
        bestTimeText_18.text = "Time: " + bestTime_18;
    }

    public void ReturnMenuButton()
    {
        highScoresPanel.SetActive(false);
        menuPanel.SetActive(true);
    }

    // Game preperation before playing
    private void StartGame(int startNumber)
    {
        // Create ids array that it has card ids twice.
        int[] ids = new int[startNumber];
        int index = 0;
        int id = 1;
        while (index < startNumber - 1)
        {
            ids[index] = id;
            index++;
            ids[index] = id;
            id++;
            index++;
        }

        // Shuffle ids array to make a puzzle
        randomize(ids, startNumber);

        // Activation and setting color of specific cards for 6, 12 and 18 cards
        if (startNumber == 6)
        {
            for (int i = 0; i < startNumber; i++)
            {
                cards[indexesStart6[i]].GetComponent<CardId>().id = ids[i];
                cards[indexesStart6[i]].GetComponent<CardId>().color = ChooseColor(ids[i]);
                cards[indexesStart6[i]].SetActive(true);
            }
        }
        else if (startNumber == 12)
        {
            for (int i = 0; i < startNumber; i++)
            {
                cards[indexesStart12[i]].GetComponent<CardId>().id = ids[i];
                cards[indexesStart12[i]].GetComponent<CardId>().color = ChooseColor(ids[i]);
                cards[indexesStart12[i]].SetActive(true);
            }
        }

        else if (startNumber == 18)
        {
            for (int i = 0; i < startNumber; i++)
            {
                cards[i].GetComponent<CardId>().id = ids[i];
                cards[i].GetComponent<CardId>().color = ChooseColor(ids[i]);
                cards[i].SetActive(true);
            }
        }

        else
        {
            Debug.Log("Error...");
        }

        timeManager.SetActive(true);
    }


    void randomize(int[] arr, int n)
    {
        // Start from the last element and 
        // swap one by one. We don't need to 
        // run for the first element  
        // that's why i > 0 
        for (int i = n - 1; i > 0; i--)
        {

            // Pick a random index 
            // from 0 to i 
            int j = Random.Range(0, i + 1);

            // Swap arr[i] with the 
            // element at random index 
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }

    // Setting color according to id
    private Color ChooseColor(int id)
    {
        switch (id)
        {
            case 1:
                return Color.red;
            case 2:
                return Color.yellow;
            case 3:
                return Color.cyan;
            case 4:
                return Color.grey;
            case 5:
                return Color.black;
            case 6:
                return Color.magenta;
            case 7:
                return Color.white;
            case 8:
                return new Color(0.7f, 1, 0.1f, 255);
            case 9:
                return new Color(0.8f, 0.4f, 0.1f, 255);
            default:
                return Color.clear;
            

        }

    }

    // When click any card in game
    public void OnClickCard()
    {
        GameObject card = EventSystem.current.currentSelectedGameObject;
        card.GetComponent<Image>().sprite = cardBack;
        card.GetComponent<Image>().color = card.GetComponent<CardId>().color;
        card.GetComponent<Button>().interactable = false;
        Debug.Log(card.GetComponent<CardId>().id);

        if (selectedCards == 0)
        {
            selectedCardsArray[0] = card;
            selectedCards++;
        }
        else if (selectedCards == 1)
        {
            selectedCardsArray[1] = card;
            StartCoroutine(CheckCoupling());
        }
    }

    // Checking cards and for result: getting points or cards will be back old positions
    private IEnumerator CheckCoupling()
    {
        InteractableCloseButtons();
        attempt++;
        attemptText.text = "Attempt: " + attempt;

        yield return new WaitForSeconds(1);
        selectedCards = 0;

        // If two ids are equal
        if (selectedCardsArray[0].GetComponent<CardId>().id == selectedCardsArray[1].GetComponent<CardId>().id)
        {
            succeedAttempt++;

            if (IsGameDone() == 1)
            {
                Debug.Log("Game Over");
                GameEnding();
            }

            selectedCardsArray[0].SetActive(false);
            selectedCardsArray[1].SetActive(false);

            InteractableOpenButtons();

        }
        else
        {
            selectedCardsArray[0].GetComponent<Image>().color = Color.white;
            selectedCardsArray[0].GetComponent<Image>().sprite = cardFront;

            selectedCardsArray[1].GetComponent<Image>().color = Color.white;
            selectedCardsArray[1].GetComponent<Image>().sprite = cardFront;

            InteractableOpenButtons();
        }


    }

    // 1 is success 0 is not success
    private byte IsGameDone()
    {
        if (succeedAttempt == startNumber / 2)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    // Game Ending function
    private void GameEnding()
    {
        timeManager.GetComponent<TimeManager>().InactiveTimeObject();
        attemptText.text = "";


        finishAttempt.text = "Attempt: " + attempt;

        float time = timeManager.GetComponent<TimeManager>().GetTime();
        Debug.Log(time);
        finishTime.text = string.Format("{0:#.00} second", time);

        bool isHighScoreNull = false;

        // For start number best scores operations will be changed
        if (startNumber == 6)
        {
            int tempMaxAttempt = PlayerPrefs.GetInt("MAX_ATTEMPT_6");
            float tempMaxTime = PlayerPrefs.GetFloat("MAX_TIME_6");

            if (tempMaxAttempt == 0)
            {
                isHighScoreNull = true;
            }

            if (isHighScoreNull)
            {
                PlayerPrefs.SetInt("MAX_ATTEMPT_6", attempt);
                PlayerPrefs.SetFloat("MAX_TIME_6", time);
            }
            else
            {
                if(attempt < tempMaxAttempt)
                {
                    PlayerPrefs.SetInt("MAX_ATTEMPT_6", attempt);
                }
                if(time < tempMaxTime)
                {
                    PlayerPrefs.SetFloat("MAX_TIME_6", time);
                }
            }

           
        }
        else if (startNumber == 12)
        {
            int tempMaxAttempt = PlayerPrefs.GetInt("MAX_ATTEMPT_12");
            float tempMaxTime = PlayerPrefs.GetFloat("MAX_TIME_12");

            if (tempMaxAttempt == 0)
            {
                isHighScoreNull = true;
            }

            if (isHighScoreNull)
            {
                PlayerPrefs.SetInt("MAX_ATTEMPT_12", attempt);
                PlayerPrefs.SetFloat("MAX_TIME_12", time);
            }
            else
            {
                if (attempt < tempMaxAttempt)
                {
                    PlayerPrefs.SetInt("MAX_ATTEMPT_12", attempt);
                }
                if (time < tempMaxTime)
                {
                    PlayerPrefs.SetFloat("MAX_TIME_12", time);
                }
            }


        }
        else if (startNumber == 18)
        {
            int tempMaxAttempt = PlayerPrefs.GetInt("MAX_ATTEMPT_18");
            float tempMaxTime = PlayerPrefs.GetFloat("MAX_TIME_18");

            if (tempMaxAttempt == 0)
            {
                isHighScoreNull = true;
            }

            if (isHighScoreNull)
            {
                PlayerPrefs.SetInt("MAX_ATTEMPT_18", attempt);
                PlayerPrefs.SetFloat("MAX_TIME_18", time);
            }
            else
            {
                if (attempt < tempMaxAttempt)
                {
                    PlayerPrefs.SetInt("MAX_ATTEMPT_18", attempt);
                }
                if (time < tempMaxTime)
                {
                    PlayerPrefs.SetFloat("MAX_TIME_18", time);
                }
            }
        }
        else
        {
            Debug.Log("something is bad");
        }

        timeManager.GetComponent<TimeManager>().PauseTime();
        timeManager.GetComponent<TimeManager>().ResetTime();

        escMenuButton.SetActive(false);

        finishGamePanel.SetActive(true);     
    }




    // Game Start with 6 cards
    public void Start6()
    {
        startNumber = 6;
        StartGame(startNumber);
        menuPanel.SetActive(false);
        transitionPanel.SetActive(false);
    }

    // Game Start with 12 cards
    public void Start12()
    {
        startNumber = 12;
        StartGame(startNumber);
        menuPanel.SetActive(false);
        transitionPanel.SetActive(false);
    }

    // Game Start with 18 cards
    public void Start18()
    {
        startNumber = 18;
        StartGame(startNumber);
        menuPanel.SetActive(false);
        transitionPanel.SetActive(false);
    }

    // Make cards interactable = false
    private void InteractableCloseButtons()
    {
        for (int i = 0; i < cards.Length; i++)
        {
            cards[i].GetComponent<Button>().interactable = false;
        }
    }

    // Make cards interactable = true
    private void InteractableOpenButtons()
    {
        for (int i = 0; i < cards.Length; i++)
        {
            cards[i].GetComponent<Button>().interactable = true;
        }
    }

}